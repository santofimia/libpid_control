cmake_minimum_required(VERSION 2.8.3)

set(PROJECT_NAME pid_control)
project(${PROJECT_NAME})


### Use version 2011 of C++ (c++11). By default ROS uses c++98
#see: http://stackoverflow.com/questions/10851247/how-to-activate-c-11-in-cmake
#see: http://stackoverflow.com/questions/10984442/how-to-detect-c11-support-of-a-compiler-with-cmake
add_definitions(-std=c++11)
#add_definitions(-std=c++0x)
#add_definitions(-std=c++03)
add_definitions(-g)

# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries
#set(ROS_BUILD_TYPE RelWithDebInfo)
#SET(CMAKE_BUILD_TYPE Release) # Release, RelWithDebInfo


set(PID_CONTROL_SOURCE_DIR
        src/source)

set(PID_CONTROL_INCLUDE_DIR
        src/include)

set(PID_CONTROL_HEADER_FILES
        ${PID_CONTROL_INCLUDE_DIR}/pid_control.h)

set(PID_CONTROL_SOURCE_FILES
        ${PID_CONTROL_SOURCE_DIR}/pid_control.cpp)


find_package(catkin REQUIRED
                COMPONENTS roscpp pugixml lib_cvgutils)


catkin_package(
	INCLUDE_DIRS ${PID_CONTROL_INCLUDE_DIR}
        LIBRARIES pid_control
        CATKIN_DEPENDS roscpp pugixml lib_cvgutils
  )



include_directories(${PID_CONTROL_INCLUDE_DIR})
include_directories(${catkin_INCLUDE_DIRS})


add_library(pid_control ${PID_CONTROL_SOURCE_FILES} ${PID_CONTROL_HEADER_FILES})
add_dependencies(pid_control ${catkin_EXPORTED_TARGETS})
target_link_libraries(pid_control ${catkin_LIBRARIES})

