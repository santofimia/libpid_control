#ifndef pid_control
#define pid_control


/*!*************************************************************************************
 *  \class     PID
 *
 *  \brief     PID
 *
 *  \details   This class defines the controller PID.
 *
 *
 *  \authors   Pablo Santofimia Ruiz
 *
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ***************************************************************************************/

#include "Timer.h"
#include <ros/ros.h>

class PID {
private:
    // Input/output
    float reference;
    float feedback;
    float output;

    // Parameters
    float kp, ki, kd, kw;
    float fl;
    bool  antiwindup_enabled, saturation_enabled, feedforward_enabled;
    float antiwindup_min, antiwindup_max;
    float saturation_min, saturation_max;
    float feedforward_value;

    // Internal state
    float integrator;
    float last_error;
    float error;
    float average;
    float elapsed;
    Timer timer;
    bool started;

public:
    PID();
    ~PID();

    inline void setGains(const float p, const float i, const float d) { kp = p; ki = i; kd = d; }
    inline void setDerFactor(const float d){fl = d;}
    inline void getGains(float &p, float &i, float &d) { p = kp; i = ki; d = kd; }
    inline void setInt(float integ) { integrator = integ; }
    inline float getInt() { return integrator; }
    inline float getError() {return error = reference - feedback; }
    void enableMaxOutput(bool enable, float max);
    void enableAntiWindup(bool enable, const float w);
    void enableMaxOutput(bool enable, float min, float max);
    void enableFeedForward(bool enable, float mass, float fffactor);

    inline void setReference(float ref) { reference = ref; }
    inline void setFeedback(float measure) { feedback = measure; }
    float getOutput();

    void reset();
};
#endif /* pid_control */
